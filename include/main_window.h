/** @file main_window.h
    @brief Main window class */
#ifndef QRV_MAIN_WINDOW_H
#define QRV_MAIN_WINDOW_H

#include <string>

#include <Qt/qapplication.h>
#include <Qt/qmainwindow.h>
#include <Qt/qscrollarea.h>
#include <Qt/qscrollbar.h>
#include <Qt/qlabel.h>
#include <Qt/qaction.h>
#include <Qt/qmenu.h>
#include <Qt/qmenubar.h>
#include <Qt/qstatusbar.h>
#include <Qt/qtoolbar.h>
#include <Qt/qstyle.h>
#include <Qt/qmessagebox.h>

#include "download.h"

/** @class MainWindow
    @brief Main window class */
class MainWindow : public QMainWindow {
	Q_OBJECT

	public:
		MainWindow();

		/**
			@brief Shows an image in the main window
			@param[in] s_path Reference to the path of the image to be shown
			@return True on success, false otherwise
		*/
		bool show_image( const std::string &s_path );

	public slots:
		/**
			@brief Downloads a random picture via google
		*/
		void download();
		/**
			@brief Deletes all downloaded pictures
		*/
		void delete_pics();

		/**
			@brief Zooms out by 25\%
		*/
		void zoom_out();
		/**
			@brief Zooms in by 25\%
		*/
		void zoom_in();
		/**
			@brief Fits image to window
		*/
		void fit_to_window();
		/**
			@brief Restores normal size of the image
		*/
		void normal_size();
		/**
			@brief Shows freshly downloaded pictures
		*/
		void show_pics();

	private:
		/**
			@brief Creates actions
		*/
		void create_actions();
		/**
			@brief Updates actions
		*/
		void update_actions();
		/**
			@brief Creates menus
		*/
		void create_menus();
		/**
			@brief Creates toolbars
		*/
		void create_toolbars();
		/**
			@brief Creates statusbar
		*/
		void create_statusbar();

		/**
			@brief Scales image by a factor
			@param[in] f_factor Factor
		*/
		void scale_image( double f_factor );

		/**
			@brief Adjusts scrollbars according to scale factor
			@param[in] p_scrollbar Pointer to the scrollbar to be adjusted
			@param[in] f_factor Factor by which the scrollbars should be adjusted
		*/
		void adjust_scrollbar( QScrollBar *p_scrollbar, double f_factor );

        //! Image downloader
        Downloader m_download;

		//! Path of the most recently downloaded picture
		std::string m_image_path;

		//! Scroll area widget
		QScrollArea m_scroll_area;
		//! Image preview window widget
		QLabel m_preview;
		//! Image preview window scale factor
		double m_scale;

		//! Status label on the statusbar
		QLabel m_status;

		//! Download action
		QAction *ma_download;
		//! Delete pics action
		QAction *ma_delete;
		//! Exit action
		QAction *ma_exit;

		//! Fit to window action
		QAction *ma_fit_to_window;
		//! Normal size action
		QAction *ma_normal_size;
		//! Zoom in action
		QAction *ma_zoom_in;
		//! Zoom out action
		QAction *ma_zoom_out;
		//! Show pics action
		QAction *ma_show_pics;

		//! File menu
		QMenu *mm_file;
		//! View menu
		QMenu *mm_view;

		//! Toolbar
		QToolBar *mt_bar;
};

#endif

