/** @file main.cpp
    @brief The main function */
#include <iostream>

#include <Qt/qapplication.h>

#include "main_window.h"

int main (int argc, char *argv []) {
    try {
	QApplication app( argc, argv );
	MainWindow win;

	win.show();

        return app.exec ();
    } catch (...) {
        std::cout << "Memory allocation failed.." << std::endl;
        return EXIT_FAILURE;
    }
}
